# IADF-2023-SAR-processing
## Synthetic Aperture Radar Data Analysis
### Florence Tupin, Emanuele Dalsasso

This repository contains the jupyter notebook we will use during the hands-on part of the course.

The [presentation](https://partage.imt.fr/index.php/s/7FFaMbb6RcRYQYT) is also made available.
